google-cloud-pubsub==1.7.0
google-cloud-storage==1.31.0
flask==1.1.2
flask_cors==3.0.9
gunicorn==20.0.4